<?php require_once("header.php");

	if(isset($_POST['submit'])) {
		unset($_POST['submit']);

		$sql = sprintf("INSERT INTO " .$_GET['table'] ." (%s) VALUES ('%s')",
    				implode(',',array_keys($_POST)),
    				implode("','",array_values($_POST))
				);

		if(!($result = $mysql->query($sql))) {
			printf("<span class=\"error\">$mysql->error</span>");
		} else {
			echo $sql;
		}
	}

	if(isset($_GET['table'])) {
		 $tableName = $_GET['table'];
	}
	else {
		$tableName = 'BOOK';
	}

	printf("<h1>Table: ".$tableName."</h1>");

	if($result = $mysql->query("SELECT * FROM ".$tableName)) {
		displayResult($result);

		$columns = $result->fetch_fields();

		printf("<h3>Insert a new $tableName</h3>");
		printf("<form action='".$_SERVER['REQUEST_URI']."' method='post'>");

		foreach($columns as $column) {

			$placeholder = $column->name;

			$placeholder = $placeholder." (".h_type2txt($column->type).") ";
			$placeholder = $placeholder." (".h_flags2txt($column->flags).")";

			if(! ($column->flags & 512) ) {
				printf("<input type='text' name='$column->name'
					placeholder='$placeholder'");
				if($column->flags & 1) {
					echo 'required ';
				}

				printf("/>");
			}
		}
		printf("<input name='submit' type='submit' value='insert'/>");
		printf("</form>");

		$result-> close();
	} else {
		echo $mysql->error;
	}

	require_once("footer.php");
?>
