<?php include("db_connect.php"); ?>
<!DOCTYPE html>
<head>
	<title>CS 457 - Assignment 5</title>
	<link href="style.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/styles/default.min.css">
</head>
<body>
	<div id="wrapper">
	<header>
		<h1>Deliverables</h1>
		<ul>
			<li><a href="sql/ddl.txt">Task 1</a></li>
			<li><a href="sql/data.txt">Task 2</a></li>
			<li><a href="task32.php">Task 3.2</a></li>
		</ul>
		<h1>Tables</h1>
		<ul>
			<li><a href="table.php?table=LIBRARIAN">LIBRARIAN</a></li>
			<li><a href="table.php?table=AUTHOR">AUTHOR</a></li>
			<li><a href="table.php?table=GENRE">GENRE</a></li>
			<li><a href="table.php?table=LIBRARY">LIBRARY</a></li>
			<li><a href="table.php?table=MEMBER">MEMBER</a></li>
			<li><a href="table.php?table=BOOK">BOOK</a></li>
			<li><a href="table.php?table=TOPIC">TOPIC</a></li>
			<li><a href="table.php?table=HAS">HAS</a></li>
			<li><a href="table.php?table=CHECKS_OUT">CHECK_OUT</a></li>
			<li><a href="table.php?table=WRITES">WRITES</a></li>
		</ul>
		<form action="query.php" method="post">
			<p style="font-weight:800;">Note: PHP-MySQL queries are case sensitive.</p>
			<textarea name="query" placeholder="enter a sql query"><?php printf("%s", trim($_POST['query']));?></textarea>
			<input type="submit" value="submit"/>
		</form>
	</header>
