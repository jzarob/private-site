<?php require_once("header.php");

	function selected($name, $op) {
		if(strcmp($op, $name) == 0) {
			return "selected";
		} else {
			return "";
		}
	}

	if(isset($_POST['query'])) {
		$query = $_POST['query'];
		$operator = "";
		if(isset($_POST['val'])) {
			$operator = html_entity_decode($_POST["operator"]);


			$query = sprintf("%s WHERE %s %s '%s'",
								$query, $_POST['field'],
								$operator, $_POST['val']);
		}

		if($result = $mysql->query($query)) {
			printf("<form action='query.php' method='post'>");
			printf("<input type='hidden' name='query' value='%s'/>", $_POST['query']);
			printf("<label>%s</label>", "Predicate Conditions:");

			printf("<select name='%s'>", "field");
			$fields = $result->fetch_fields();
		    foreach($fields as $field) {
				$fname = sprintf("%s.%s",$field->table, $field->name);
		        printf("<option value='%s' %s>%s</option>",
					$fname, selected($fname, $_POST["field"]), $field->name);
		    }
			printf("<select>");
			printf("<select name='operator'>");
			printf("<option value='=' %s>=</option>", selected("=", $operator));
			printf("<option value='&gt;' %s>&gt;</option>", selected(">", $operator));
			printf("<option value='&lt;' %s>&lt;</option>", selected("<", $operator));
			printf("</select>");

			printf('<input type="text" name="val" placeholder="value"
						value="%s"/>', $_POST['val']);
			printf("<input type='submit' name='submit' value='filter'/>");
			printf("</form>");

			displayResult($result);

			$result->close();
		} else {
			printf("<span class=\"error\">$mysql->error</span>");
		}

	} else {
		echo 'We did not get a query. :(';
	}

	require_once("footer.php");
?>
