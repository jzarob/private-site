<?php

require_once("header.php");

printf("<form action='query.php' method='post'>");
?>

<select name="query">
	<option value="SELECT B.title, A.full_name FROM BOOK B INNER JOIN WRITES W ON B.isbn = W.isbn INNER JOIN AUTHOR A ON W.author_id = A.id">
		Books and their authors
	</option>
	<option value="SELECT M.full_name, M.birthday, L.name FROM MEMBER M INNER JOIN LIBRARY L ON M.belongs_to = L.id">
		Members and the library they belong to
	</option>
</select>

<input type="submit" value="submit"/>
<?php
printf("</form>");
require_once("footer.php");
 ?>
