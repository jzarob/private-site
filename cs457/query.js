// TASK 1
// Find how many records are in the table.

print("\nTASK 1:");
db.HW6.find().count();

// TASK 2
// List all fields (including _id) for events of type PublicEvent.
// Print using pretty().

print("\nTASK 2:");
db.HW6.find({"type":"PublicEvent"}).pretty();

// TASK 3
// Print 2. again using printjson() in a shell script.
// Define a cursor to use in the printjson().
print("\nTASK 3:");
cursor = db.HW6.find({"type":"PublicEvent"});
while(cursor.hasNext()) {
    printjson(cursor.next());
}

// TASK 4
// Using the cursor method count, return a count of the number
// of documents that are a GollumEvent .
print("\nTASK 4:");
db.HW6.find({"type":"GollumEvent"}).count();

// TASK 5
// List the member login and member id of the payload for members
// who have an event type of MemberEvent, and use the cursor sort
// to sort the result by the member's login.
print("\nTASK 5:");
db.HW6.find({"type":"MemberEvent"},
            {
                "payload.member.login":1,
                "payload.member.id":1,
                "_id":0
            }).sort({"payload.member.login":1});
// TASK 6
// For all type Member Events that have an actor id < 1200000,
// list its actor id and repo name.
print("\nTASK 6:");
db.HW6.find({"type":"MemberEvent", "actor.id": {$lt:1200000}},
            {
                "actor.id":1,
                "repo.name":1,
                "_id":0
            });

// TASK 7
// For all type Member Events that have an actor id < 1200000 or > 10000000,
// list its actor id and repo name.
print("\nTASK 7:");
db.HW6.find({
                "type":"MemberEvent",
                $or:
                    [
                        {"actor.id": {$lt:1200000}},
                        {"actor.id": {$gt:10000000}}
                    ]
            },
            {
                "actor.id":1,
                "repo.name":1,
                "_id":0
            });

// TASK 8
// Count the number of events that have a labels color field
// for the payload issue.
print("\nTASK 8:");
db.HW6.find({
            "payload.issue.labels": {
                $elemMatch : {"color":{ $exists : true}}
            }
        }).count();

// TASK 9
// For all documents in the collection with a public field,
// determine if any of the events are NOT public. e.g. false.
print("\nTASK 9");
db.HW6.find({
    $and:[
        {"public" : {$exists :true}},
        {"public" : false}
    ]
}).count();

// TASK 10
// For all events that have an id > 2489678837 and do not have a
// field for the payload issue user, list its id.
print("\nTASK 10:");
db.HW6.find({
    $and : [
        {"id": {$gt: "2489678837"}},
        {"payload.issue.user" : {$exists : false}}
    ]
}, {"id":1, "_id":0});

// TASK 11
// List the maximum id value (not the _id).  Note do not use $max.
print("\nTASK 11:");
db.HW6.find({}, {"id":1, "_id":0}).sort({"id":-1}).limit(1);

// TASK 12
// Using pipeline aggregation, group the documents by payload pull_request
// status to find the maximum value of the payload issue number.
// Print each payload pull_request state and its maximum value.

print("\nTASK 12:");

var test = db.HW6.aggregate([
    {
        $group : {
            "_id":"$payload.pull_request.state",
            "max_value": { $max: "$payload.issue.number" }
        }
    }
]);

test;


// TASK 13
// Use the aggregate sort to order the result from question 12.
// by payload pull_request status
print("\nTASK 13:");
db.HW6.aggregate([
        {
            $group : {
                "_id":"$payload.pull_request.state",
                "max_value": { $max: "$payload.issue.number" }
            }
        },
        { $sort : {"payload.pull_request.state":-1}}
    ]
);

// TASK 14
// Using the single purpose aggregation operation of group, group the documents
// by payload pull_request state to count the number of occurrences of each
// payload pull_request state.  Print each payload pull_request state  and its
// count.  There is an example of this in the MongoDB documentation - look for
// Single Purpose Aggregation Operations. You just have to tweak it a little.
print("\nTASK 14:");
db.HW6.aggregate(
    [
        {
            $group : {
                "_id":"$payload.pull_request.state",
                "count": { $sum: 1}
            }
        }
    ]
);
