<?php
require_once("connect_strings.php");

$mysql = new mysqli($host, $username, $password, $database, $port);
if ($mysql->connect_errno) {
    echo $mysql->connect_error;
    exit();
}

// BEGIN CODE TAKEN FROM http://php.net/manual/en/mysqli-result.fetch-fields.php
function h_type2txt($type_id) {
    static $types;

    if (!isset($types)) {
        $types = array();
        $constants = get_defined_constants(true);
        foreach ($constants['mysqli'] as $c => $n) {
             if (preg_match('/^MYSQLI_TYPE_(.*)/', $c, $m)) {
                  $types[$n] = $m[1];
             }
        }
    }

    return array_key_exists($type_id, $types)? $types[$type_id] : NULL;
}

function h_flags2txt($flags_num)
{
    static $flags;

    if (!isset($flags)) {
        $flags = array();
        $constants = get_defined_constants(true);
        foreach ($constants['mysqli'] as $c => $n) {
             if (preg_match('/MYSQLI_(.*)_FLAG$/', $c, $m)) {
                  if (!array_key_exists($n, $flags)) $flags[$n] = $m[1];
             }
        }
    }

    $result = array();
    foreach ($flags as $n => $t) if ($flags_num & $n) $result[] = $t;
    return implode(' ', $result);
}

// END CODE TAKEN FROM WEB

function displayResult($result) {
    $fields = $result->fetch_fields();

    printf("<table>");

    printf("<tr>");
    foreach($fields as $field) {
        printf("<th>$field->name</th>");
    }
    printf("</tr>");

    while($row = $result->fetch_assoc()) {
        printf("<tr>");

        foreach($fields as $field) {
            printf("<td>".$row[$field->name]."</td>");
        }

        printf("</tr>");
    }

    printf("</table>");
}

 ?>
